
all: build

CHARTDIR?=$(CURDIR)/charts/
CHARTS=$(notdir $(wildcard $(CHARTDIR)/*))

test:
	echo $(CHARTS)

%.pkg: charts/%/* charts/%/**/*
	helm package $(CHARTDIR)/$* && touch $@

.PHONY: build
build: URL?=https://gitlab.com/mulesoft-int/helm-repository/raw/master/
build: $(addsuffix .pkg, $(CHARTS))
	helm repo index --url=$(URL)  $(CURDIR)

.PHONY: publish
publish: build
	git add .
	git commit -m 'New chart version'
	git push
