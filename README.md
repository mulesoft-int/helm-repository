# Helm Repository


## How to install a chart
```sh
$ kubectl run -n kube-system --serviceaccount=tiller -ti helm-hello-world --image=registry.gitlab.com/mulesoft-int/helm-repository --restart=Never --rm  -- upgrade --install --atomic --namespace default hello-world local/istio-helloworld
```

## How to remove a chart
```sh
$ kubectl run -n kube-system --serviceaccount=tiller -ti helm-hello-world --image=registry.gitlab.com/mulesoft-int/helm-repository --restart=Never --rm  -- delete hello-world
```