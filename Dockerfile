FROM alpine:3.8 as extract
RUN apk add -U curl ca-certificates
ARG ARCH=linux-amd64 
ARG VERSION=v3.2.4
RUN curl https://get.helm.sh/helm-${VERSION}-${ARCH}.tar.gz | tar xvzf - --strip-components=1 -C /usr/bin

FROM alpine:3.8
RUN apk add -U --no-cache ca-certificates jq bash
COPY --from=extract /usr/bin/helm /usr/bin/

ENV HELM_REPOSITORY_CACHE=/helm/repository/local HELM_REPOSITORY_CONFIG=/helm/repository/repositories.yaml

COPY entry /usr/bin/
COPY ./*.tgz ${HELM_REPOSITORY_CACHE}/
COPY ./index.yaml  ${HELM_REPOSITORY_CACHE}/local-index.yaml
COPY ./repositories.yaml ${HELM_REPOSITORY_CONFIG}

ENTRYPOINT ["entry"]

