{{- $name := .Release.Name -}}
{{- $version := .Release.Revision | quote -}}
{{- $pathDefault := printf "/%s(/.*)" $name -}}
---
apiVersion: v1
kind: Service
metadata:
  name: {{ $name }}
  labels:
    app: {{ $name }}
    version: {{ $version }}
spec:
  type: {{ .Values.service.type }}
  ports:
  - name: http-{{ $name }}
    port: {{ .Values.service.port }}
    targetPort: {{ .Values.service.targetPort }}
  selector:
    app: {{ $name }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $name }}
  labels:
    app: {{ $name }}
    version: {{ $version }}
spec:
  selector:
    matchLabels:
      app: {{ $name }}

  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0

  replicas: {{ required "deployment.replicas.count is required" .Values.deployment.replicas.count }}

  template:
    metadata:
      name: {{ $name }}
      labels:
        app: {{ $name }}
        version: {{ $version }}
    spec:
      containers:
      - name: app
        image: {{ .Values.image.name }}
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        ports:
        - name: app
          containerPort: {{.Values.service.targetPort}}
          protocol: TCP
        resources:
        {{- toYaml .Values.resources | nindent 10 }}

{{- if .Values.ingress.enabled }}
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ $name }}
  labels:
    app: {{ $name }}
    version: {{ $version }}
  {{- if .Values.ingress.name }}
  annotations:
    kubernetes.io/ingress.class: {{ .Values.ingress.name }}
  {{- end }}
spec:
  {{- if .Values.ingress.name }}
  ingressClassName: {{ .Values.ingress.name }}
  {{- end }}
  rules:
  - http:
      paths:
      - path: {{ default $pathDefault .Values.ingress.path }}
        pathType: ImplementationSpecific
        backend:
          service:
            name: {{ $name }}
            port:
              number: {{ .Values.service.port }}
{{- end }}