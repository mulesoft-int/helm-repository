{{/*
Calculate Config Version.
*/}}
{{- define "config.version" -}}
{{- . | print | sha256sum | trunc 8 -}}
{{- end -}}

{{- define "config.name" -}}
{{- include "base.name" . -}}-{{ template "config.version" .Values.deployment.env }}
{{- end -}}

{{- define "secret.name" -}}
{{- include "base.name" . -}}-{{ template "config.version" .Values.deployment.secretEnv }}
{{- end -}}

{{- define "base.name" -}}
{{- $suf := default .Chart.Name .Values.nameSuffix -}}
{{- $name := printf "%s-%s" .Release.Name $suf -}}
{{- $name | lower | trunc 54 | trimSuffix "-" -}}
{{- end -}}

{{- define "base.namespace" -}}
{{- .Release.Namespace -}}
{{- end -}}

{{- define "base.service.port" -}}
{{- .Values.service.port -}}
{{- end -}}

{{- define "base.service.type" -}}
{{- .Values.service.type -}}
{{- end -}}